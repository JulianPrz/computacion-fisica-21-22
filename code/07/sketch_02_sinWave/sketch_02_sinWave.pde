float diameter; 
float angle = 0;
float d;

void setup() {
  size(640, 360);
  diameter = height - 10;
  noStroke();
}

void draw() {
  background(0);
  d = 10 + (sin(angle) * diameter/2) + diameter/2;
  fill(255, 204, 0);
  ellipse(width/2, height/2, d, d);
  fill(255);
  text("Sin: "+sin(angle), 15, height*3/4, 64);
  text("Diameter: "+d, 15, height*3/4+15, 64);
  text("Angle: "+angle, 15, height*3/4+30, 64);
  angle += 0.02;
  if (angle >= TWO_PI) {
    angle=0;
  }
}
