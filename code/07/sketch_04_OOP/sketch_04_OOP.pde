Ball b;

void setup(){
  size(600,600);
  b = new Ball(random(width), random(height), 15, color(255));
}

void draw(){
  background(0);
  b.display();
  b.move(); //Añadimos funcionalidad
  b.checkEdges();
}
