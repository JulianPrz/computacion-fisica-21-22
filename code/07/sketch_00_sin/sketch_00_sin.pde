float r = 150;
float x;
float y;
float a = 0;

void setup(){
  size(500,500);
  background(0);
  noFill();
}
void draw(){
   translate(width/2, height/2);
   stroke(255);
   strokeWeight(4);
   beginShape();
   for(int i = 0; a < TWO_PI; a+=0.1){ //Si mapeamos el incremento tendremos una interacción

     x = r * cos(a);
     y = r * sin(a);
     vertex(x,y);
   }
   endShape(CLOSE);
 }
