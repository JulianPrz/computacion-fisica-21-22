Ball[] b = new Ball[20];
int nBalls = 4; //Definimos una variable para el número de bolas

void setup() {
  size(600, 600);
  for (int i = 0; i < b.length; i++) {
    b[i] = new Ball(random(width), random(height), 15, color(255));
  }
}

void mousePressed(){
  nBalls++; //sumamos bolas cuando clickamos
}

void keyPressed(){
  nBalls--; //restamos bolas cuando tecleamos
}

void draw() {
  background(0);
  for (int i = 0; i < nBalls; i++) {
    b[i].display();
    b[i].move();
    b[i].checkEdges();
  }
}
