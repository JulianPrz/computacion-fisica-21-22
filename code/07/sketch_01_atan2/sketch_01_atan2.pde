void setup(){
  size(400,400);
  rectMode(CENTER);
  textSize(30);
}

void draw() {
  background(0);
  pushMatrix();
  translate(width/2, height/2);
  float a = atan2(mouseY-height/2, mouseX-width/2);
  rotate(a);
  fill(255);
  rect(0, 0, 300, 10);
  popMatrix();
  text(int(degrees(a))+"º",20,height-40);
}
