PImage im[] = new PImage[10];
String imFile[] = {"0.jpg", "1.jpg", "2.jpg", "3.jpg", "4.jpg", "5.jpg", "6.jpg", "7.jpg", "8.jpg", "9.jpg", };
int wIm = 125;
int hIm = 218;

void settings(){
  size(wIm*2, hIm*3);
}

void setup() {
  for (int i = 0; i < 10; i = i+1) {
    im[i] = loadImage(imFile[i]);
  }
}

void draw() {
  
  /////////////////HORAS/////////////////////////////
  
  int h = hour();
  int h_dec = int(h/10);
  int h_uni = h - h_dec *10;

  image(im[h_dec], 0, 0);
  image(im[h_uni], wIm, 0);

  ////////////////MINUTOS////////////////////////////

  int m = minute();
  int m_dec = int(m/10);
  int m_uni = m - m_dec *10;

  image(im[m_dec], 0, hIm);
  image(im[m_uni], wIm, hIm);

  ////////////////SEGUNDOS///////////////////////////

  int s = second();
  int s_dec = int(s/10);
  int s_uni = s - s_dec *10;

  image(im[s_dec], 0, hIm*2);
  image(im[s_uni], wIm, hIm*2);
}
